package jp.masakura.textlint.process

import jp.masakura.intellij.file.TargetFile
import jp.masakura.textlint.report.TextlintReport

interface Textlint {
    fun scan(file: TargetFile): TextlintReport
    fun fix(file: TargetFile): String
    fun isSupported(file: TargetFile): Boolean
}
