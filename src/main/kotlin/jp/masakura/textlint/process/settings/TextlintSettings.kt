package jp.masakura.textlint.process.settings

import jp.masakura.textlint.command.TextlintCommand
import jp.masakura.textlint.file.FileSystem
import jp.masakura.textlint.process.Textlint
import jp.masakura.textlint.process.TextlintImpl
import jp.masakura.textlint.process.TextlintrcTextlint

data class TextlintSettings(
    val excludeFiles: TextlintExcludeFiles,
    val filesToScan: TextlintSettingsFilesToScan,
) {
    fun createTextlint(
        workDirectory: String,
        command: TextlintCommand,
        files: FileSystem,
    ): Textlint {
        val textlint = TextlintImpl(workDirectory, command)

        return TextlintrcTextlint(
            excludeFiles.wrap(
                filesToScan.wrap(textlint, command),
                workDirectory,
                files,
            ),
            workDirectory,
            files,
        )
    }

    companion object {
        val DEFAULT: TextlintSettings =
            TextlintSettings(
                TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE,
                TextlintSettingsFilesToScan.USE_PRINT_CONFIG,
            )
    }
}
