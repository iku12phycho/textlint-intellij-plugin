package jp.masakura.textlint.process.settings

import jp.masakura.intellij.file.TargetFile
import jp.masakura.textlint.command.TextlintCommand
import jp.masakura.textlint.configuration.PrintConfig
import jp.masakura.textlint.process.Textlint
import jp.masakura.textlint.report.TextlintReport

/**
 * Select the files to scan based on the results of `textlint --print-config`.
 */
class PrintConfigFilesToScanFilterTextlint(private val textlint: Textlint, private val command: TextlintCommand) :
    Textlint {
    private val printConfig by lazy {
        PrintConfig.create(command)
    }

    override fun scan(file: TargetFile): TextlintReport {
        return textlint.scan(file)
    }

    override fun fix(file: TargetFile): String {
        return textlint.fix(file)
    }

    override fun isSupported(file: TargetFile): Boolean {
        return printConfig.get().isSupported(file)
    }

    override fun toString(): String {
        return "PrintConfigTextlint -> $textlint"
    }
}
