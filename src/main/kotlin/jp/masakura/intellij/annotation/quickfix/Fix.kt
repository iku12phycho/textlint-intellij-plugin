package jp.masakura.intellij.annotation.quickfix

import com.intellij.openapi.editor.Document

interface Fix {
    fun fix(document: Document)
}
