package jp.masakura.intellij.annotation

import com.intellij.lang.annotation.AnnotationHolder
import com.intellij.lang.annotation.ExternalAnnotator
import com.intellij.openapi.editor.Editor
import com.intellij.psi.PsiFile
import jp.masakura.intellij.annotation.cache.Cache
import jp.masakura.intellij.annotation.issue.AnnotationScanner
import jp.masakura.intellij.annotation.issue.Annotations
import jp.masakura.intellij.annotation.label.AnnotatorLabel
import jp.masakura.intellij.annotation.quickfix.AnnotationFixer
import jp.masakura.intellij.annotation.quickfix.QuickFix
import jp.masakura.intellij.annotation.target.Target
import jp.masakura.intellij.annotation.target.toTarget

abstract class ExternalAnnotatorBase(
    private val scanner: AnnotationScanner,
    private val fixer: AnnotationFixer,
    private val label: AnnotatorLabel,
) :
    ExternalAnnotator<Target, Annotations>() {
    private val cache = Cache()

    override fun collectInformation(file: PsiFile): Target {
        return file.toTarget()
    }

    override fun collectInformation(file: PsiFile, editor: Editor, hasErrors: Boolean): Target {
        return collectInformation(file)
    }

    override fun doAnnotate(collectedInfo: Target?): Annotations? {
        if (collectedInfo == null) return null

        return cache.getOrInvoke(collectedInfo) { annotate(it) }
    }

    override fun apply(file: PsiFile, annotationResult: Annotations?, holder: AnnotationHolder) {
        if (annotationResult == null) return

        annotationResult.appendTo(holder)
    }

    private fun annotate(collectedInfo: Target): Annotations {
        val fixCurrentFile = QuickFix.forCurrentFile(fixer, collectedInfo)
        return scanner.scan(collectedInfo).addQuickFixCurrentFile(fixCurrentFile).appendLabel(label)
    }
}
