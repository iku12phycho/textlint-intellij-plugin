package jp.masakura.textlint.command

import com.intellij.execution.configurations.GeneralCommandLine

class TextlintCommand(
    private val workDirectory: String,
    private val command: () -> GeneralCommandLine = { GeneralCommandLine() },
    private val npx: Npx = Npx.DEFAULT,
) {
    fun execute(parameters: List<String>): Process {
        val c =
            command()
                .withExePath(npx.command)
                .withWorkDirectory(workDirectory)
                .withParameters(listOf("textlint") + parameters)

        return c.createProcess()
    }
}
