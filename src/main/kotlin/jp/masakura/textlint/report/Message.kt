package jp.masakura.textlint.report

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class Message(
    val type: String,
    val ruleId: String,
    val message: String,
    val index: Int,
    val line: Int,
    val column: Int,
    val range: List<Int>,
    val loc: Location,
    val severity: SeverityLevel,
    val fix: Fix? = null,
)
