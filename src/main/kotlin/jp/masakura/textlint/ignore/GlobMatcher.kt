package jp.masakura.textlint.ignore

import java.nio.file.Path

interface GlobMatcher {
    fun matches(relativePath: Path): Boolean
    fun matches(relativePath: String): Boolean {
        return matches(Path.of(relativePath))
    }

    companion object {
        val EMPTY: GlobMatcher = EmptyGlobMatcher.INSTANCE

        fun create(pattern: String): GlobMatcher {
            return SmartPathMatcherGlobMatcher.create(pattern)
        }

        fun createAll(patterns: List<String>): GlobMatcher {
            return GlobMatchers(patterns.map { create(it) })
        }
    }
}
