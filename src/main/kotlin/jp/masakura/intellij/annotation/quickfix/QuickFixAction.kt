package jp.masakura.intellij.annotation.quickfix

import com.intellij.codeInsight.intention.impl.BaseIntentionAction
import com.intellij.openapi.command.WriteCommandAction
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.project.Project
import com.intellij.psi.PsiDocumentManager
import com.intellij.psi.PsiFile

class QuickFixAction(
    private val quickFix: QuickFix,
) : BaseIntentionAction() {
    override fun getText(): String {
        return quickFix.text.toString()
    }

    override fun getFamilyName(): String {
        return "TEXTLINT"
    }

    override fun isAvailable(project: Project, editor: Editor?, file: PsiFile?): Boolean {
        return true
    }

    override fun invoke(project: Project, editor: Editor?, file: PsiFile?) {
        val document = PsiDocumentManager.getInstance(project).getDocument(file!!)!!
        WriteCommandAction.writeCommandAction(project).run<Exception> {
            quickFix.fix(document)
        }
    }
}
