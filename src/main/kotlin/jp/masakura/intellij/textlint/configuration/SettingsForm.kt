package jp.masakura.intellij.textlint.configuration

import jp.masakura.textlint.process.settings.TextlintExcludeFiles
import jp.masakura.textlint.process.settings.TextlintSettingsFilesToScan
import javax.swing.JPanel
import javax.swing.JRadioButton

class SettingsForm : PluginSettings {
    lateinit var root: JPanel
    private lateinit var dontUseTextlintIgnoreRadioButton: JRadioButton
    private lateinit var useTextlintIgnoreRadioButton: JRadioButton
    private lateinit var usePrintConfigRadioButton: JRadioButton
    private lateinit var ignorePrintConfigRadioButton: JRadioButton

    override var excludeFiles: TextlintExcludeFiles
        get() {
            if (useTextlintIgnoreRadioButton.isSelected) return TextlintExcludeFiles.USE_TEXTLINT_IGNORE
            return TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE
        }
        set(value) {
            excludeFilesRadioButton(value).isSelected = true
        }
    override var printConfig: TextlintSettingsFilesToScan
        get() {
            if (usePrintConfigRadioButton.isSelected) return TextlintSettingsFilesToScan.USE_PRINT_CONFIG
            return TextlintSettingsFilesToScan.ANY_FILES
        }
        set(value) {
            printConfigRadioButton(value).isSelected = true
        }

    private fun excludeFilesRadioButton(value: TextlintExcludeFiles): JRadioButton {
        return when (value) {
            TextlintExcludeFiles.DONT_USE_TEXTLINT_IGNORE -> dontUseTextlintIgnoreRadioButton
            TextlintExcludeFiles.USE_TEXTLINT_IGNORE -> useTextlintIgnoreRadioButton
        }
    }

    private fun printConfigRadioButton(value: TextlintSettingsFilesToScan): JRadioButton {
        return when (value) {
            TextlintSettingsFilesToScan.USE_PRINT_CONFIG -> usePrintConfigRadioButton
            TextlintSettingsFilesToScan.ANY_FILES -> ignorePrintConfigRadioButton
        }
    }
}
