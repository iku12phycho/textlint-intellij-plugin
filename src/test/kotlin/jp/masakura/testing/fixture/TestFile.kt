package jp.masakura.testing.fixture

import java.nio.file.Files
import java.nio.file.Path

class TestFile(val path: String, val content: String) {
    companion object {
        fun load(relativePath: String): TestFile {
            val path = Path.of(System.getProperty("user.dir")).resolve(relativePath)
            return TestFile(
                path.toString().replace("\\", "/"),
                Files.readString(path, Charsets.UTF_8),
            )
        }
    }
}
