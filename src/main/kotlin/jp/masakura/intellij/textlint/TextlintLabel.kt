package jp.masakura.intellij.textlint

import jp.masakura.intellij.annotation.label.AnnotatorLabel

data class TextlintLabel(private val value: String) : AnnotatorLabel {
    override fun toString(): String {
        return value
    }

    override fun appendTo(s: String): String {
        return "$value: $s"
    }

    companion object {
        val instance = TextlintLabel("textlint")
    }
}
