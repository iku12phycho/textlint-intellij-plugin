package jp.masakura.intellij.annotation.issue

import jp.masakura.intellij.annotation.target.Target

interface AnnotationScanner {
    fun scan(target: Target): Annotations
}
