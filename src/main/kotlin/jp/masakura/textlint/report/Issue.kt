package jp.masakura.textlint.report

data class Issue(val messages: List<Message>, val filePath: String)
