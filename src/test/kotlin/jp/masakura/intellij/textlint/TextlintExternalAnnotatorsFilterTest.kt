package jp.masakura.intellij.textlint

import com.intellij.lang.annotation.ExternalAnnotator
import jp.masakura.intellij.textlint.configuration.PluginSettingsState
import jp.masakura.testing.file.FakeTextlintFactory
import jp.masakura.testing.fixture.Fixtures
import jp.masakura.testing.project.FakeProject
import jp.masakura.testing.project.FakePsiFile
import jp.masakura.textlint.process.settings.TextlintExcludeFiles
import jp.masakura.textlint.process.settings.TextlintSettings
import jp.masakura.textlint.process.settings.TextlintSettingsFilesToScan
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class TextlintExternalAnnotatorsFilterTest {
    private lateinit var rst: FakePsiFile
    private lateinit var javascript: FakePsiFile
    private lateinit var html: FakePsiFile
    private lateinit var markdown: FakePsiFile
    private lateinit var project: FakeProject

    @BeforeEach
    fun setUp() {
        val state =
            PluginSettingsState.from(
                TextlintSettings(
                    TextlintExcludeFiles.USE_TEXTLINT_IGNORE,
                    TextlintSettingsFilesToScan.USE_PRINT_CONFIG,
                ),
            )
        project = FakeProject.create("/project1", state)
        markdown = project.file("sample.md", "- [ ] TODO")
        html = project.file("sample.html", "<p>TODO</p>")
        rst = project.file("sample.rst", "**TODO**")
        javascript = project.file("sample.js", """console.log("hello")""")
    }

    @Test
    fun `allows execution of TextlintExternalAnnotator with markdown`() {
        val target =
            FakeTextlintFactory()
                .printedConfig(Fixtures.printedConfigs.DEFAULT)
                .createExternalAnnotator()

        assertFalse(target.isProhibited(TextlintExternalAnnotator(), markdown))
    }

    @Test
    fun `prohibit execution of TextlintExternalAnnotator with html, by default`() {
        val target =
            FakeTextlintFactory()
                .printedConfig(Fixtures.printedConfigs.DEFAULT)
                .createExternalAnnotator()

        assertTrue(target.isProhibited(TextlintExternalAnnotator(), html))
    }

    @Test
    fun `allows execution of TextlintExternalAnnotator with html`() {
        val target =
            FakeTextlintFactory()
                .printedConfig(Fixtures.printedConfigs.HTML)
                .createExternalAnnotator()

        assertFalse(target.isProhibited(TextlintExternalAnnotator(), html))
    }

    @Test
    fun `allows execution of TextlintExternalAnnotator with rst`() {
        val target =
            FakeTextlintFactory()
                .printedConfig(Fixtures.printedConfigs.RST)
                .createExternalAnnotator()

        assertFalse(target.isProhibited(TextlintExternalAnnotator(), rst))
    }

    @Test
    fun `prohibit execution of TextlintExternalAnnotator when textlintrc not exists`() {
        val target =
            FakeTextlintFactory()
                .printedConfig(Fixtures.printedConfigs.EMPTY)
                .createExternalAnnotator()

        assertTrue(target.isProhibited(TextlintExternalAnnotator(), markdown))
    }

    @Test
    fun `prohibit execution of TextlintExternalAnnotator when include textlintignore`() {
        val target =
            FakeTextlintFactory()
                .printedConfig(Fixtures.printedConfigs.DEFAULT)
                .textlintIgnore("**/*.md")
                .createExternalAnnotator()

        assertTrue(target.isProhibited(TextlintExternalAnnotator(), project.file("foo/bar.md", "")))
    }

    @Test
    fun `prohibit execution of TextlintExternalAnnotator when psiFile is null`() {
        val target = FakeTextlintFactory().createExternalAnnotator()

        assertTrue(target.isProhibited(TextlintExternalAnnotator(), null))
    }

    @Test
    fun `allows execution of another ExternalAnnotator when textlintrc exists`() {
        val target = FakeTextlintFactory().createExternalAnnotator()

        assertFalse(target.isProhibited(DummyExternalAnnotator(), markdown))
    }

    @Test
    fun `allows execution of another ExternalAnnotator when textlintrc not exists`() {
        val target = FakeTextlintFactory().createExternalAnnotator()

        assertFalse(target.isProhibited(DummyExternalAnnotator(), markdown))
    }

    @Test
    fun `allows execution of another ExternalAnnotator when textlintrc not exists (Unkown FileType)`() {
        val target = FakeTextlintFactory().createExternalAnnotator()

        assertFalse(target.isProhibited(DummyExternalAnnotator(), javascript))
    }

    private fun FakeTextlintFactory.createExternalAnnotator(): TextlintExternalAnnotatorsFilter {
        return TextlintExternalAnnotatorsFilter(this)
    }
}

class DummyExternalAnnotator : ExternalAnnotator<String, String>()
