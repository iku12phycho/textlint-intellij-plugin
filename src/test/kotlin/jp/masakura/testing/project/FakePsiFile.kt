package jp.masakura.testing.project

import com.intellij.openapi.fileTypes.FileType
import com.intellij.psi.PsiFile
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

abstract class FakePsiFile : PsiFile {
    companion object {
        fun create(
            myProject: FakeProject,
            relativePath: String,
            myContent: String,
            myFileType: FileType,
            myModificationCount: Long,
        ): FakePsiFile {
            val myVirtualFile = FakeVirtualFile.create(
                myProject.projectDir,
                relativePath,
                myModificationCount,
            )

            return mock<FakePsiFile> {
                on { project } doReturn myProject
                on { text } doReturn myContent
                on { virtualFile } doReturn myVirtualFile
                on { fileType } doReturn myFileType
            }
        }
    }
}
