package jp.masakura.intellij.textlint

import com.intellij.codeInsight.intention.IntentionAction

class IntentionActions(private val list: List<IntentionAction>) {
    fun first(): IntentionAction {
        return list[0]
    }

    fun forCurrentFile(): IntentionAction {
        return list[1]
    }

    fun texts(): List<String> {
        return list.map { it.text }
    }

    companion object {
        fun from(annotations: List<AnnotationResult>): IntentionActions {
            return IntentionActions(annotations.flatMap { it.quickFixes.list })
        }
    }
}
