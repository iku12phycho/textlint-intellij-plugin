#!/bin/bash

RELEASE_FILE_PATH=$(ls build/libs/textlint*.jar | head -1)
RELEASE_FILE=$(basename "$RELEASE_FILE_PATH")

echo "RELEASE_FILE_PATH=$RELEASE_FILE_PATH" > variables.env
echo "RELEASE_FILE=$RELEASE_FILE" >> variables.env
echo "RELEASE_FILE_SLUG=${RELEASE_FILE/+/-}" >> variables.env