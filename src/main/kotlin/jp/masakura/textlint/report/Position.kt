package jp.masakura.textlint.report

data class Position(val line: Int, val column: Int)
