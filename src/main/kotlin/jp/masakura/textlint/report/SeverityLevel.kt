package jp.masakura.textlint.report

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.annotation.JsonDeserialize

@JsonDeserialize(using = SeverityLevelDeserializer::class)
enum class SeverityLevel {
    Information,
    Error,
    Warning,
}

class SeverityLevelDeserializer : JsonDeserializer<SeverityLevel>() {
    override fun deserialize(parser: JsonParser?, context: DeserializationContext?): SeverityLevel {
        return when (parser?.codec?.readValue(parser, Int::class.java)) {
            0 -> SeverityLevel.Information
            1 -> SeverityLevel.Warning
            2 -> SeverityLevel.Error

            else -> throw IllegalArgumentException("Severity level require 0, 1, 2")
        }
    }
}
