package jp.masakura.textlint.configuration.processor

class Processors(private val processors: List<Processor>) {
    fun isSupported(extension: String): Boolean {
        return processors.any { it.isSupported(extension) }
    }
}
