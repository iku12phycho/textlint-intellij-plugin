package jp.masakura.intellij.textlint

import jp.masakura.intellij.annotation.issue.AnnotationScanner
import jp.masakura.intellij.annotation.issue.Annotations
import jp.masakura.intellij.annotation.target.Target
import jp.masakura.intellij.textlint.configuration.textlintSettings
import jp.masakura.textlint.process.TextlintFactory

class TextlintScannerAdapter(private val factory: TextlintFactory = TextlintFactory.DEFAULT) :
    AnnotationScanner {
    override fun scan(target: Target): Annotations {
        val scanner = factory.create(target.projectPath, target.textlintSettings())
        return scanner.scan(target.file).toAnnotations()
    }
}
