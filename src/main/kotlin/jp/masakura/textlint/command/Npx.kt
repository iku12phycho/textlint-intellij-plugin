package jp.masakura.textlint.command

import java.util.*

class Npx(val command: String) {
    companion object {
        private val WINDOWS = Npx("npx.cmd")
        private val NOT_WINDOWS = Npx("npx")
        val DEFAULT = from(System.getProperty("os.name"))

        fun from(osName: String): Npx {
            if (osName.lowercase(Locale.ENGLISH).startsWith("windows")) return WINDOWS
            return NOT_WINDOWS
        }
    }
}
