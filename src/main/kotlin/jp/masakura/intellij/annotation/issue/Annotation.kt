package jp.masakura.intellij.annotation.issue

import com.intellij.lang.annotation.AnnotationHolder
import jp.masakura.intellij.annotation.label.AnnotatorLabel
import jp.masakura.intellij.annotation.quickfix.QuickFix
import jp.masakura.intellij.annotation.quickfix.QuickFixes

data class Annotation(
    val issue: Issue,
    private val fixes: List<QuickFix> = emptyList(),
) {
    private val quickFixes
        get() = QuickFixes(fixes)

    fun addQuickFixCurrentFile(fixCurrentFile: QuickFix): Annotation {
        return Annotation(issue, quickFixes.addQuickFixCurrentFile(fixCurrentFile).toList())
    }

    fun appendLabel(label: AnnotatorLabel): Annotation {
        return Annotation(issue.appendLabel(label), quickFixes.appendLabel(label).toList())
    }

    fun appendTo(holder: AnnotationHolder) {
        val builder = holder.newAnnotation(issue.severity, issue.message.toString())
            .range(issue.range)

        quickFixes.appendTo(builder)

        builder.create()
    }
}
