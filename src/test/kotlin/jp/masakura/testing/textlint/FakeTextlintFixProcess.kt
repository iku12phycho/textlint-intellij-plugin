package jp.masakura.testing.textlint

import com.intellij.openapi.util.io.FileUtil
import java.io.File
import java.io.InputStream
import java.io.OutputStream
import java.text.Normalizer

class FakeTextlintFixProcess private constructor(private val filePath: String) : Process() {
    override fun getOutputStream(): OutputStream {
        throw IllegalStateException("Not supported")
    }

    override fun getInputStream(): InputStream {
        throw IllegalStateException("Not supported")
    }

    override fun getErrorStream(): InputStream {
        throw IllegalStateException("Not supported")
    }

    override fun waitFor(): Int {
        val file = File(filePath)
        val content = FileUtil.loadFile(file, Charsets.UTF_8)
        val normalized = Normalizer.normalize(content, Normalizer.Form.NFC)
        FileUtil.writeToFile(file, normalized, Charsets.UTF_8)

        return 0
    }

    override fun exitValue(): Int {
        throw IllegalStateException("Not supported")
    }

    override fun destroy() {
        throw IllegalStateException("Not supported")
    }

    companion object {
        fun create(options: TextlintCommandLineOptions): FakeTextlintFixProcess? {
            if (options.fix) return FakeTextlintFixProcess(options.filePath)
            return null
        }
    }
}
