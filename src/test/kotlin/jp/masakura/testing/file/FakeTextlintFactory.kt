package jp.masakura.testing.file

import jp.masakura.testing.fixture.Fixture
import jp.masakura.testing.textlint.FakeTextlintGeneralCommandLineBuilder
import jp.masakura.textlint.command.TextlintCommand
import jp.masakura.textlint.process.Textlint
import jp.masakura.textlint.process.TextlintFactory
import jp.masakura.textlint.process.settings.TextlintExcludeFiles
import jp.masakura.textlint.process.settings.TextlintSettings
import jp.masakura.textlint.process.settings.TextlintSettingsFilesToScan

class FakeTextlintFactory(
    private val command: FakeTextlintGeneralCommandLineBuilder = FakeTextlintGeneralCommandLineBuilder(),
    private val files: FakeFileSystemBuilder = FakeFileSystemBuilder(),
) : TextlintFactory {
    override fun create(
        workDirectory: String,
        settings: TextlintSettings,
    ): Textlint {
        return settings.createTextlint(
            workDirectory,
            TextlintCommand(workDirectory, { command.build() }),
            files.build(workDirectory),
        )
    }

    fun createUseTextlintIgnore(workDirectory: String): Textlint {
        return create(
            workDirectory,
            TextlintSettings(
                TextlintExcludeFiles.USE_TEXTLINT_IGNORE,
                TextlintSettingsFilesToScan.USE_PRINT_CONFIG,
            ),
        )
    }

    fun add(fixture: Fixture): FakeTextlintFactory {
        return FakeTextlintFactory(command.add(fixture), files)
    }

    fun printedConfig(
        value: String,
        textlintrcRelativePath: String = ".textlintrc.json",
    ): FakeTextlintFactory {
        return FakeTextlintFactory(command.setPrintedConfig(value), files.add(FakeFile(textlintrcRelativePath, value)))
    }

    fun textlintIgnore(globs: String): FakeTextlintFactory {
        return FakeTextlintFactory(command, files.add(FakeFile(".textlintignore", globs)))
    }
}
