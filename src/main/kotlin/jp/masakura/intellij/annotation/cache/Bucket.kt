package jp.masakura.intellij.annotation.cache

import jp.masakura.intellij.annotation.issue.Annotations
import jp.masakura.intellij.annotation.target.Target

class Bucket(private val modificationCount: Long, val annotations: Annotations) {
    fun modified(target: Target): Boolean {
        return modificationCount != target.modificationCount
    }

    companion object {
        fun from(target: Target, annotations: Annotations): Bucket {
            return Bucket(target.modificationCount, annotations)
        }
    }
}

private val Target.modificationCount: Long
    get() {
        return this.file.modificationCount
    }
