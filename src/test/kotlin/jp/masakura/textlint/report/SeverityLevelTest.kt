package jp.masakura.textlint.report

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class SeverityLevelTest {
    private lateinit var deserializer: ObjectMapper

    @BeforeEach
    fun setUp() {
        deserializer = jacksonObjectMapper()
    }

    @Test
    fun testInfo() {
        val result = deserializer.readValue<SeverityLevel>("0")

        assertEquals(SeverityLevel.Information, result)
    }

    @Test
    fun testWarn() {
        val result = deserializer.readValue<SeverityLevel>("1")

        assertEquals(SeverityLevel.Warning, result)
    }

    @Test
    fun testError() {
        val result = jacksonObjectMapper().readValue<SeverityLevel>("2")

        assertEquals(SeverityLevel.Error, result)
    }
}
