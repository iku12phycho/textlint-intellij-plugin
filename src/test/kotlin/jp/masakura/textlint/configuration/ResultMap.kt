package jp.masakura.textlint.configuration

import jp.masakura.intellij.file.TargetFile

data class ResultMap(
    private val map: Map<String, Boolean>,
) {
    override fun toString(): String {
        val buffer = StringBuffer()
        for (entry in map) {
            buffer.appendLine("${entry.key} => ${entry.value}")
        }
        return buffer.toString()
    }

    fun getIsSupportedResult(target: PrintedConfig): ResultMap {
        return ResultMap((map.keys.associateWith { target.isSupported(TargetFile(it, "")) }))
    }
}
