package jp.masakura.intellij.textlint

import jp.masakura.intellij.annotation.quickfix.AnnotationFixer
import jp.masakura.intellij.annotation.target.Target
import jp.masakura.intellij.textlint.configuration.textlintSettings
import jp.masakura.textlint.process.TextlintFactory

class TextlintFixerAdapter(private val factory: TextlintFactory = TextlintFactory.DEFAULT) : AnnotationFixer {
    override fun fix(target: Target): String {
        val textlint = factory.create(target.projectPath, target.textlintSettings())
        return textlint.fix(target.file)
    }
}
