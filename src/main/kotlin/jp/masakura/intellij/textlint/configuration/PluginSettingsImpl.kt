package jp.masakura.intellij.textlint.configuration

import jp.masakura.textlint.process.settings.TextlintExcludeFiles
import jp.masakura.textlint.process.settings.TextlintSettings
import jp.masakura.textlint.process.settings.TextlintSettingsFilesToScan

data class PluginSettingsImpl(
    override var excludeFiles: TextlintExcludeFiles,
    override var printConfig: TextlintSettingsFilesToScan,
) : PluginSettings {
    companion object {
        fun default(): PluginSettingsImpl =
            PluginSettingsImpl(
                TextlintSettings.DEFAULT.excludeFiles,
                TextlintSettings.DEFAULT.filesToScan,
            )
    }
}
