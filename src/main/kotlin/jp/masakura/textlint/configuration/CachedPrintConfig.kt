package jp.masakura.textlint.configuration

class CachedPrintConfig(private val printConfig: PrintConfig) : PrintConfig {
    private var cache: PrintedConfig? = null

    override fun get(): PrintedConfig {
        if (cache == null) {
            cache = printConfig.get()
        }
        return cache!!
    }
}
