package jp.masakura.testing.textlint

import jp.masakura.testing.fixture.Fixture
import jp.masakura.testing.fixture.Fixtures

class FakeTextlintGeneralCommandLineBuilder(
    private val fixtures: List<Fixture> = emptyList(),
    private val printedConfig: String = Fixtures.printedConfigs.EMPTY,
) {
    fun add(fixture: Fixture): FakeTextlintGeneralCommandLineBuilder {
        return FakeTextlintGeneralCommandLineBuilder(fixtures + listOf(fixture), printedConfig)
    }

    fun setPrintedConfig(value: String): FakeTextlintGeneralCommandLineBuilder {
        return FakeTextlintGeneralCommandLineBuilder(fixtures, value)
    }

    fun build(): FakeTextlintGeneralCommandLine {
        return FakeTextlintGeneralCommandLine(fixtures, printedConfig)
    }
}
