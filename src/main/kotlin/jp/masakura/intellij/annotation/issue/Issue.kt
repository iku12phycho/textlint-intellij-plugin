package jp.masakura.intellij.annotation.issue

import com.intellij.lang.annotation.HighlightSeverity
import com.intellij.openapi.util.TextRange
import jp.masakura.intellij.annotation.label.AnnotatorLabel

data class Issue(
    val ruleId: String,
    val severity: HighlightSeverity,
    val message: IssueMessage,
    val range: TextRange,
) {
    fun appendLabel(label: AnnotatorLabel): Issue {
        return Issue(ruleId, severity, message.appendLabel(label), range)
    }

    constructor(ruleId: String, severity: HighlightSeverity, message: String, range: TextRange) : this(
        ruleId,
        severity,
        IssueMessage(message),
        range,
    )
}
