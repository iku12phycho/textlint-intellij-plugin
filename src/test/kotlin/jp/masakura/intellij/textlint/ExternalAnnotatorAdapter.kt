package jp.masakura.intellij.textlint

import com.intellij.codeInsight.daemon.impl.AnnotationHolderImpl
import com.intellij.lang.annotation.AnnotationSession
import com.intellij.lang.annotation.ExternalAnnotator
import com.intellij.psi.PsiFile
import jp.masakura.intellij.annotation.issue.Annotations
import jp.masakura.intellij.annotation.target.Target

class ExternalAnnotatorAdapter(private val target: ExternalAnnotator<Target, Annotations>) {
    fun perform(psiFile: PsiFile): AnnotationResults {
        val collectInformation = target.collectInformation(psiFile)
        val annotations = target.doAnnotate(collectInformation)

        val annotationHolder = AnnotationHolderImpl(AnnotationSession(psiFile), true)
        annotationHolder.applyExternalAnnotatorWithContext(psiFile, target, annotations)

        return AnnotationResults.from(annotationHolder)
    }
}
