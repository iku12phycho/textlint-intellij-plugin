package jp.masakura.textlint.ignore

import java.nio.file.Path

class GlobMatchers(private val matchers: List<GlobMatcher>) : GlobMatcher {
    override fun matches(relativePath: Path): Boolean {
        return matchers.any { it.matches(relativePath) }
    }
}
