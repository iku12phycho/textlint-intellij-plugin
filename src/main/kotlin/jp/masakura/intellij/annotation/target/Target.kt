package jp.masakura.intellij.annotation.target

import com.intellij.openapi.project.Project
import com.intellij.psi.PsiFile
import jp.masakura.intellij.file.TargetFile

class Target private constructor(
    val file: TargetFile,
    val project: Project,
) {
    val projectPath = project.basePath!!

    companion object {
        fun from(file: PsiFile): Target {
            return Target(file.toTargetFile(), file.project)
        }
    }
}

fun PsiFile.toTarget(): Target {
    return Target.from(this)
}

fun PsiFile.toTargetFile(): TargetFile {
    return TargetFile(virtualFile.path, text, fileType, virtualFile.modificationCount)
}
