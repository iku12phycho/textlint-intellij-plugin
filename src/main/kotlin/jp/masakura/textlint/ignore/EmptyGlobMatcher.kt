package jp.masakura.textlint.ignore

import java.nio.file.Path

class EmptyGlobMatcher private constructor() : GlobMatcher {
    override fun matches(relativePath: Path): Boolean {
        return false
    }

    companion object {
        val INSTANCE: GlobMatcher = EmptyGlobMatcher()
    }
}
