package jp.masakura.intellij.annotation.label

interface AnnotatorLabel {
    fun appendTo(s: String): String
}
