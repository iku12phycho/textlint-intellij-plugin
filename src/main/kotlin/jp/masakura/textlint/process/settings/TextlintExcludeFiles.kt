package jp.masakura.textlint.process.settings

import jp.masakura.textlint.file.FileSystem
import jp.masakura.textlint.process.Textlint

enum class TextlintExcludeFiles {
    DONT_USE_TEXTLINT_IGNORE {
        override fun wrap(
            textlint: Textlint,
            workDirectory: String,
            files: FileSystem,
        ): Textlint {
            return textlint
        }
    },
    USE_TEXTLINT_IGNORE {
        override fun wrap(
            textlint: Textlint,
            workDirectory: String,
            files: FileSystem,
        ): Textlint {
            return TextlintIgnoreTextlint(textlint, workDirectory, files)
        }
    },
    ;

    abstract fun wrap(
        textlint: Textlint,
        workDirectory: String,
        files: FileSystem,
    ): Textlint
}
