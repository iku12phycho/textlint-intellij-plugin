package jp.masakura.textlint.ignore

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.junit.jupiter.params.provider.ValueSource

class GlobMatcherTest {
    private lateinit var create: (String) -> GlobMatcher

    @BeforeEach
    fun setUp() {
        create = { GlobMatcher.create(it) }
    }

    @Test
    fun `filename (match)`() {
        val target = create("file.txt")

        assertTrue(target.matches("file.txt"))
    }

    @ParameterizedTest
    @ValueSource(strings = ["file1.txt", "file.md"])
    fun `filename (not match)`(filename: String) {
        val target = create("file.txt")

        assertFalse(target.matches(filename))
    }

    @ParameterizedTest
    @ValueSource(strings = ["file1.txt", "file2.txt"])
    fun `wildcard (match)`(filename: String) {
        val target = create("*.txt")

        assertTrue(target.matches(filename))
    }

    @Test
    fun `wildcard (not match)`() {
        val target = create("*.txt")

        assertFalse(target.matches("file1.md"))
    }

    @ParameterizedTest
    @ValueSource(strings = ["foo/file.txt", "foo/bar/file.txt"])
    fun `globstar (match)`(filename: String) {
        val target = create("**/file.txt")

        assertTrue(target.matches(filename))
    }

    @ParameterizedTest
    @CsvSource(
        "**/file.txt, file.txt",
        "*/**/file.txt, src/file.txt",
        "foo/**/bar/**/file.txt, foo/bar/file.txt",
        "foo/**/bar/**/file.txt, foo/bar/src/file.txt",
        "foo/**/bar/**/file.txt, foo/src/bar/file.txt",
    )
    fun `globstar no directory (match)`(pattern: String, path: String) {
        val target = create(pattern)

        assertTrue(target.matches(path))
    }

    @ParameterizedTest
    @ValueSource(strings = ["file1.txt", "file2.txt"])
    fun `character class (match)`(filename: String) {
        val target = create("file[123].txt")

        assertTrue(target.matches(filename))
    }

    @Test
    fun `character class (not match)`() {
        val target = create("file[123].txt")

        assertFalse(target.matches("file4.txt"))
    }

    @ParameterizedTest
    @ValueSource(strings = ["file1.txt", "file2.txt"])
    fun `brace expansion 1 (match)`(filename: String) {
        val target = create("{file1,file2}.txt")

        assertTrue(target.matches(filename))
    }

    @ParameterizedTest
    @ValueSource(strings = [".textlintrc", ".textlintrc.json"])
    fun `brace expansion 2 (match)`(filename: String) {
        val target = create(".textlintrc{,.json}")

        assertTrue(target.matches(filename))
    }
}
