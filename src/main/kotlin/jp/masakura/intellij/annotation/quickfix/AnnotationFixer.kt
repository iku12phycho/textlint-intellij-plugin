package jp.masakura.intellij.annotation.quickfix

import jp.masakura.intellij.annotation.target.Target

interface AnnotationFixer {
    fun fix(target: Target): String
}
