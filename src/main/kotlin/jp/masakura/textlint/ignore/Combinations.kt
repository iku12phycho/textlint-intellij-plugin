package jp.masakura.textlint.ignore

fun <T> List<T>.combinations(n: Int): List<List<T>> {
    if (n <= 1) return this.map { listOf(it) }
    if (this.count() <= n) return listOf(this)
    return this.drop(1).combinations(n - 1).map { listOf(this.first()) + it } +
        this.drop(1).combinations(n)
}

fun <T> List<T>.combinationsAll(): List<List<T>> {
    return this.combinationsAll(this.count())
}

fun <T> List<T>.combinationsAll(n: Int): List<List<T>> {
    return (n downTo 1).flatMap { this.combinations(it) }
}
