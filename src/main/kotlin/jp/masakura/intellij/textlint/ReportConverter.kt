package jp.masakura.intellij.textlint

import com.intellij.openapi.util.TextRange
import jp.masakura.intellij.annotation.issue.Annotation
import jp.masakura.intellij.annotation.issue.Annotations
import jp.masakura.intellij.annotation.issue.Issue
import jp.masakura.intellij.annotation.issue.IssueMessage
import jp.masakura.intellij.annotation.quickfix.FixPart
import jp.masakura.intellij.annotation.quickfix.QuickFix
import jp.masakura.textlint.report.Message
import jp.masakura.textlint.report.TextlintReport

fun TextlintReport.toAnnotations(): Annotations {
    val annotations = mutableListOf<Annotation>()

    this.issues.forEach { issue ->
        run {
            issue.messages.forEach {
                annotations.add(it.toAnnotation())
            }
        }
    }
    return Annotations(annotations)
}

private fun Message.toAnnotation(): Annotation {
    return Annotation(
        createIssue(),
        createQuickFixes(),
    )
}

private fun Message.createIssue(): Issue {
    return Issue(
        ruleId,
        severity.toHighlightSeverity(),
        createIssueMessage(),
        TextRange(range[0], this.range[1]),
    )
}

private fun Message.createIssueMessage(): IssueMessage {
    return IssueMessage.create(message, ruleId)
}

private fun Message.createQuickFixes(): List<QuickFix> {
    if (fix == null) return emptyList()

    return listOf(
        QuickFix.forPart(
            FixPart(fix.text, fix.range[0], fix.range[1]),
            ruleId,
        ),
    )
}
