package jp.masakura.testing.textlint

class CommandLineParser(private val parameters: List<String>) {
    fun parseString(name: String): String? {
        val indexOf = parameters.indexOf(parameters.find { it == name })
        if (indexOf < 0) return null
        return parameters[indexOf + 1]
    }

    fun parseBoolean(name: String): Boolean {
        return parameters.find { it == name } != null
    }

    fun parseLast(): String {
        return parameters.last()
    }
}
