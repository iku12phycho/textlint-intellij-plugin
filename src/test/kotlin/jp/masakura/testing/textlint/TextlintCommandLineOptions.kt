package jp.masakura.testing.textlint

class TextlintCommandLineOptions(
    val format: String?,
    val stdin: Boolean,
    val stdinFilename: String?,
    val fix: Boolean,
    val printConfig: Boolean,
    val filePath: String,
) {
    companion object {
        fun parse(parameters: List<String>): TextlintCommandLineOptions {
            val parser = CommandLineParser(parameters)
            return TextlintCommandLineOptions(
                parser.parseString("--format"),
                parser.parseBoolean("--stdin"),
                parser.parseString("--stdin-filename"),
                parser.parseBoolean("--fix"),
                parser.parseBoolean("--print-config"),
                parser.parseLast(),
            )
        }
    }
}
