package jp.masakura.textlint.ignore

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class TextlintIgnoreParserTest {
    private lateinit var target: TextlintIgnoreParser

    @BeforeEach
    fun setUp() {
        target = TextlintIgnoreParser.INSTANCE
    }

    @Test
    fun `empty file`() {
        assertEquals(emptyList<String>(), target.parse(""))
    }

    @Test
    fun `one line`() {
        assertEquals(listOf("foo.md"), target.parse("foo.md"))
    }

    @Test
    fun `multi line`() {
        assertEquals(
            listOf("foo.md", "bar.md", "hoge.txt"),
            target.parse(
                """
                foo.md
                bar.md
                hoge.txt
                """.trimIndent(),
            ),
        )
    }

    @Test
    fun `trim whitespace`() {
        assertEquals(listOf("foo.md"), target.parse("  foo.md "))
    }

    @Test
    fun `remove empty lines`() {
        assertEquals(
            listOf("foo.md", "hoge.txt"),
            target.parse(
                """
                foo.md
                
                    
                hoge.txt
                """.trimIndent(),
            ),
        )
    }

    @Test
    fun `comment line`() {
        assertEquals(
            listOf("foo.md"),
            target.parse(
                """
                # files
                foo.md
                  # abc
                #.txt
                """.trimIndent(),
            ),
        )
    }

    @Test
    fun `comment mixed`() {
        assertEquals(listOf("foo.txt"), target.parse("foo.txt # abc"))
    }
}
