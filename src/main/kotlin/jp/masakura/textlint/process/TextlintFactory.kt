package jp.masakura.textlint.process

import jp.masakura.textlint.process.settings.TextlintSettings

interface TextlintFactory {
    fun create(
        workDirectory: String,
        settings: TextlintSettings,
    ): Textlint

    companion object {
        fun withCache(factory: TextlintFactory): TextlintFactory {
            return CachedTextlintFactory(factory)
        }

        val DEFAULT: TextlintFactory = withCache(TextlintFactoryImpl())
    }
}
